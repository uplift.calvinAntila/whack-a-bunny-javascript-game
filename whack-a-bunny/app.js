const holes = document.querySelectorAll('.hole');
const bunnies = document.querySelectorAll('.bunny');
const scoreBoardDisplay = document.querySelector('.score');
const timer = document.querySelector('.timer');
const startToPlay = document.querySelector('#start');
const highScoreDisplay = document.querySelector('.highScore');
const cursorHammer = document.querySelector('.hammer');

//variable declaration
let lastHole;
let timeUp = false;
let timeLimit = 10000;
let score = 0;
startToPlay.disabled = false;

//saving highscore to local storage
let highScore = localStorage.getItem('gameHighScore') || 0;
highScoreDisplay.textContent = highScore;

//hammer mousemove pointer
window.addEventListener('mousemove', (e) => {
    cursorHammer.style.top = `${e.pageY}px`;
    cursorHammer.style.left = `${e.pageX}px`;
});

//adding click event to each bunny to update score board
//remove the pointer event to avoid multiple click event.
bunnies.forEach(bunny => {
    bunny.addEventListener('click', () => {
        score++;
        bunny.style.backgroundImage = 'url("img/bunny2.png")';
        bunny.style.pointerEvents = 'none';
        setTimeout(() => {
            bunny.style.backgroundImage = 'url("img/bunny.png")';
            bunny.style.pointerEvents = 'all';
        }, 900)
        scoreBoardDisplay.textContent = score;
    });
});

//start game button when click
startToPlay.addEventListener('click', playGame);

//get to the random hole pass array of holes as argument
function getRandomHole(holes) {
    let randomHole = Math.floor(Math.random() * holes.length);
    const pickHole = holes[randomHole];

    while (pickHole === lastHole) {
        return pickHole;
    }

    lastHole = pickHole;
    return pickHole;
}
//display bunny to picked randomHole
function bunnyDisplay() {
    const time = Math.floor(Math.random() * 1000 + 400);
    const pickHole = getRandomHole(holes);
    pickHole.classList.add('active');
    setTimeout(function () {
        pickHole.classList.remove('active');
        if (!timeUp) {
            bunnyDisplay();
        }
    }, time);
}
//to start the game
function playGame() {
    startToPlay.disabled = true;
    bunnyDisplay();
    score = 0;
    scoreBoardDisplay.textContent = score;
    let countDown = timeLimit / 1000;
    timer.textContent = countDown;
    timeUp = false;
    setTimeout(function () {
        timeUp = true;
    }, timeLimit);

    let startTimer = setInterval(function () {
        countDown--;
        timer.textContent = countDown;
        if (countDown < 0) {
            clearInterval(startTimer);
            timer.textContent = 0;
            checkHighScore();
            startToPlay.disabled = false;
        }
    }, 1000);
}
function checkHighScore() {
    if (score > localStorage.getItem('gameHighScore')) {
        localStorage.setItem('gameHighScore', score);
        highScore = score;
        highScoreDisplay.textContent = highScore;
    }
}

